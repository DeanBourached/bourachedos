readonly OBJECTS='loader.o kmain.o hardwareDrivers/io/io.o hardwareDrivers/frame_buffer/frame_buffer.o'
readonly CC='gcc'
readonly CFLAGS='-m32 -nostdlib -nostdinc -fno-builtin-fno-stack-protector -nostartfiles -nodefaultlibs -Wall -Wextra -Werror -c'
readonly LDFLAGS='-T link.ld -melf_i386'
readonly AS='nasm'
readonly ASFLAGS='-f elf32'

$AS $ASFLAGS loader.s
$AS $ASFLAGS hardwareDrivers/io/io.s

$CC $CFLAGS -o kmain.o kmain.c
$CC $CFLAGS -o hardwareDrivers/frame_buffer/frame_buffer.o hardwareDrivers/frame_buffer/frame_buffer.c

#link all the semi-executables (.o object files)
ld $LDFLAGS $OBJECTS -o bootstrap.elf

mv bootstrap.elf iso/boot

#generate .iso file
genisoimage -R -b boot/grub/stage2_eltorito -no-emul-boot -boot-load-size 4 -A os -input-charset utf8 -quiet -boot-info-table -o bourachedOS.iso iso
bochs -f bochsrc.txt -q

#post run teardown
rm bourachedOS.iso
rm kmain.o
rm loader.o
rm iso/boot/bootstrap.elf
rm hardwareDrivers/io/io.o
rm hardwareDrivers/frame_buffer/frame_buffer.o
