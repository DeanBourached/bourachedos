#include "../io/io.h"
#include "frame_buffer.h"

/* The I/O ports */
#define FB_COMMAND_PORT         0x3D4
#define FB_DATA_PORT            0x3D5

/* The I/O port commands */
#define FB_HIGH_BYTE_COMMAND    14
#define FB_LOW_BYTE_COMMAND     15

unsigned short __fb_present_pos = 0x00000050;

void fb_write_cell(unsigned int i, char c, unsigned char bg, unsigned char fg) {

	char *fb = (char *) FRAMEBUFFER_ADDRESS;
	fb[i] = c;
    fb[i + 1] = ((bg & 0x0F) << 4) | (fg & 0x0F);
}

void fb_move_cursor(unsigned short pos) {

    outbug(FB_COMMAND_PORT, FB_HIGH_BYTE_COMMAND);
    outbug(FB_DATA_PORT,    ((pos >> 8) & 0x00FF));
    outbug(FB_COMMAND_PORT, FB_LOW_BYTE_COMMAND);
    outbug(FB_DATA_PORT,    pos & 0x00FF);
	__fb_present_pos = pos;
}

int fb_write(char *buf, unsigned int len, unsigned char bg, unsigned char fg) {

	unsigned int i;
	for(i=0; i<len; i++) {

		fb_write_cell(2*__fb_present_pos, buf[i], bg, fg);
		fb_move_cursor(__fb_present_pos + 1);

		}
	return len;
}

