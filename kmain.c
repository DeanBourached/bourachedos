#include "hardwareDrivers/frame_buffer/frame_buffer.h"

unsigned int cellIndex = 0;
unsigned int fbIndex = 0;

void incrementCellIndex() {
	fbIndex++;
	cellIndex = 2*fbIndex;
}

int kmain() {

   //setting up opening window
   //displaying manual (as opposed to default) cursor start position
   fb_write("", 1, FB_BLACK, FB_BLUE);

   char txtToDisplay[] = " booting bourachedOS.........";

   while(fbIndex < sizeof(txtToDisplay)){
		fb_write_cell(cellIndex, txtToDisplay[fbIndex], FB_WHITE, FB_BLUE);
		incrementCellIndex();
   }

   while(fbIndex < 80){
		fb_write_cell(cellIndex, ' ', FB_BROWN, FB_BLUE);
		incrementCellIndex();
   }

   incrementCellIndex();
   incrementCellIndex();
   while(fbIndex < 80*25*2){
		fb_write_cell(cellIndex, ' ', FB_BLACK, FB_BLACK);
		incrementCellIndex();
   }

   return 0;
}



